Create an AKS system with the cheapest instance as of Dec 2018.

Set the env variable GROUP to an existing AKS resource group.

```
az aks create \
    --resource-group $GROUP \
    --name cheap \
    --node-count 1 \
    --enable-addons monitoring \
    --node-vm-size Standard_A1_v2
    --generate-ssh-keys
```

Get credentials

```
az aks get-credentials --resource-group $GROUP --name cheap
```

Check current context

```
kubectl config current-context
```

Initialize Helm:

https://docs.microsoft.com/en-us/azure/aks/kubernetes-helm

Setup ingress controller:

https://docs.microsoft.com/en-us/azure/aks/ingress-tls

Get IP

```
kubectl get service -l app=nginx-ingress --namespace kube-system
```

Setup dns.


Install let's encrypt

helm install stable/cert-manager \
    --namespace kube-system \
    --set ingressShim.defaultIssuerName=letsencrypt-prod \
    --set ingressShim.defaultIssuerKind=ClusterIssuer
